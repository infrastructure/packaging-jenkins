#!/bin/sh
set -e

cp /bin/rm /bin/tar /bin/grep /usr/bin/diff /usr/local/bin
apt-get update
DPKGPATH=/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
apt-get -y -o Dpkg::Path="$DPKGPATH" --allow-remove-essential purge mktemp
apt-get -y -o Dpkg::Path="$DPKGPATH" --allow-remove-essential install coreutils tar grep diffutils findutils sed
rm /usr/local/bin/rm /usr/local/bin/tar /usr/local/bin/grep /usr/local/bin/diff
