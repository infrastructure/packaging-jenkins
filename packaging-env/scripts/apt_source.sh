#!/bin/sh

set -eu

SRCLIST=/etc/apt/sources.list
SOURCES=
MIRROR=
RELEASE=
SECURITY=
UPDATES=

opts=$(getopt -o "m:r:" -l "sources,mirror:,release:,updates,security" -- "$@")
eval set -- "$opts"

while [ $# -gt 0 ]; do
    case $1 in
        --sources) SOURCES=1; shift;;
        -m|--mirror) MIRROR="$2"; shift 2;;
        -r|--release) RELEASE="$2"; shift 2;;
        --updates) UPDATES=1; shift;;
        --security) SECURITY=1; shift;;
        --) shift; break;;
        *) ;;
    esac
done

if [ -z "$MIRROR" ] || [ -z "$RELEASE" ] || [ $# -eq 0 ]; then
    echo "Please provide mirror, release and distribution(s)." >&2
    exit 1
fi

add_entry () {
    local MIRROR=$1
    local RELEASE=$2
    local dist=$3
    local SRCLIST=$4
    local SOURCES=$5
    if ! grep -q "^deb .*$MIRROR.*$RELEASE.*$dist" $SRCLIST; then
        echo "deb $MIRROR $RELEASE $dist" >> $SRCLIST
    fi

    if test -n "$SOURCES" && ! grep -q "^deb-src .*$MIRROR.*$RELEASE.*$dist" $SRCLIST; then
        echo "deb-src $MIRROR $RELEASE $dist" >> $SRCLIST
    fi
}

for dist in $@; do
    add_entry $MIRROR $RELEASE $dist $SRCLIST "$SOURCES"
    if [ -n "$UPDATES" ]; then
        add_entry $MIRROR $RELEASE-updates $dist $SRCLIST "$SOURCES"
    fi
    if [ -n "$SECURITY" ]; then
        add_entry $MIRROR $RELEASE-security $dist $SRCLIST "$SOURCES"
    fi
done
