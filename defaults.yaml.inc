name: global
distro: 'v2020'
project: 'demo/apertis'
branch: 'apertis/v2020'
repobase: 'git@gitlab.apertis.org:'
git-credentials-id: 'df7b609b-df30-431d-a942-af263af80571'
docker-registry: docker-registry.apertis.org
