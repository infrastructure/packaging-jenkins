# Building Apertis packages from Jenkins

Apertis components are usually built using GitLab CI and OBS:

* GitLab CI generates the source package from the git repository and uploads
  them to OBS
* OBS builds the source on all the supported architectures to generate the
  binary packages

This is true for every single package in the main distribution, and is the
recommended workflow for product teams as well.

However, even if its not a generally applicable approach in some scenarios it
is desirable to use tools like Jenkins and Docker to build the binary packages,
and this repository showcases how it can be done.

## Assumptions

The workflow implemented in this repository makes the following assumption
about the components to be built:

* they have a `debian/` folder compliant with the [Debian policy
  document](https://www.debian.org/doc/debian-policy/)
* they are stored in git repositories
* they are either [packaged as native
  packages](https://wiki.debian.org/DebianMentorsFaq#What_is_the_difference_between_a_native_Debian_package_and_a_non-native_package.3F)
  or they follow the git branches layout described in the 
  [DEP-14 document](https://dep-team.paVges.debian.net/deps/dep14/), with
  upstream tarballs stored with
  [pristine-lfs](https://salsa.debian.org/debian/pristine-lfs)
* their build process and unit tests can be run in a unprivileged Docker
  container

The following items about the Jenkins installation are also assumed true:

* these plugins are enabled:
  * Pipeline
  * Git
  * Credentials
  * Docker Pipeline
  * Poll SCM
* workers have access to `/dev/kvm`
* the [QEMU user mode emulator](https://wiki.debian.org/QemuUserEmulation) for
  `aarch64`/ARM64 is installed on the workers
* transparent `aarch64` binary execution is enabled via the `binfmt_misc` mechanism
  to be able to target ARM 64-bit devices using Intel-based workers, which requires
  a Linux kernel >= 4.8 and binfmt-support >= 2.1.7, with qemu >= 1:2.12~rc3+dfsg-1
  to set the `F fix binary` flag (see
  [#868030](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=868030)), all shipped
  since Debian Buster
* `docker` is available on the workers
* `git-lfs` is available on the workers

## Workflow

The high level process is:

* sources are stored in a git repository
* Jenkins polls the git repository for changes
* the repository is checked out by Jenkins on changes
* Jenkins instantiates a Docker container based on Apertis with all the
  build-dependencies pre-installed
* the tools in the Apertis-based Docker image are used to execute the packaging
  instructions and produce binary packages
* the generated binary packages are uploaded to an artifact store

This repository also contains the reference definition of a Jenkins job to
build the Apertis-based Docker image to be used as the build environment.

## Implementation

### Building the Docker image

Create the Jenkins job:

```
$ jenkins-job update ./packaging-env.yaml
```

This creates the `/demo/apertis-v2020/packaging-env` job which points back to
this repository hosted on
https://gitlab.apertis.org/infrastructure/packaging-jenkins, checks it out
and builds the image using the instructions from `./packaging-env/Jenkinsfile`.

The build is split in two steps:
* building the base rootfs with [Debos](https://github.com/go-debos/debos)
  from the official Apertis image builder Docker image
* building the actual Docker image with the build-dependencies using the
  native Docker support in Jenkins and `./packaging-env/Dockerfile`

The current Jenkisfile applies this process for both the `amd64` and `arm64`
architectures, resulting in the creation of the
`docker-registry.apertis.org/test/v2020/amd64/packaging-env`
`docker-registry.apertis.org/test/v2020/arm64/packaging-env` images, the former
for producing Intel 64-bit binaries suitable for testing on the SDK and the
latter to build ARM 64-bit binaries to be deployed on devices.

### Building packages

The current example builds the standard `dash` packages for both Intel and ARM
64-bit targets.

Create the Jenkins jobs:

```
$ jenkins-job update ./packaging.yaml
```

This creates two jobs:
* `/demo/apertis-v2020/packaging/build-dash-amd64`
* `/demo/apertis-v2020/packaging/build-dash-arm64`

The jobs use an inline declarative pipeline to check out the sources,
pull the `packaging-env` Docker image matching the target architecture,
and then execute the build in it.

### Customizing

The job definitions make heavy use of the templating functionality
offered by `jenkins-job` and most of the general parameters can be customized
from the `defaults.yaml.inc` file.

The job definitions are meant to be customized to list the actual packages to
be built rather than `dash`, and possible extend the target architectures,
for instance by adding `armhf` for ARM 32-bit support.

The `./packaging-env/Dockerfile` needs to be customized to include all the
needed build-dependencies.

## Notes

The packaging tools can be told to auto-install the build-dependencies, trading
an easier maintenance for slower builds, but Jenkins executes its jobs as an
unprivileged user by default thus preventing the use of `apt-get`.

Workarounds may be possible but were not investigated in this set up.
